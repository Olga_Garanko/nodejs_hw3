const Truck = require('../models/truck');

const driver = (req, res, next) => {
  if (req.user.role !== 'DRIVER') {
    return res.status(403).json({message: 'Your role is not a driver'});
  }
  next();
};

const shipper = (req, res, next) => {
  if (req.user.role !== 'SHIPPER') {
    return res.status(403).json({message: 'Your role is not a shipper'});
  }
  next();
};

const enRoute = async (req, res, next) => {
  try {
    const truck = await Truck.findOne({status: 'OL', assigned_to: req.user._id});
    if (truck) {
      return res.status(403).json({message: 'User is en route'});
    }
    next();
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports = {
  driver,
  shipper,
  enRoute
};
