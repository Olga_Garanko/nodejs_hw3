const {truckTypes} = require('../constants');
const Truck = require('../models/truck');

const getTrucks = (req, res) => {
  Truck.find({created_by: req.user._id})
      .exec()
      .then((trucks) => res.status(200).json({trucks}))
      .catch((err) => res.status(500).json({message: err.message}));
};

const getTruck = async (req, res) => {
  try {
    const truck = await Truck.findOne({_id: req.params.id, created_by: req.user._id});
    if (!truck) {
      return res.status(400).json({message: 'There is no truck with such id'});
    }
    res.status(200).json({truck});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

const deleteTruck = async (req, res) => {
  try {
    const truck = Truck.findOne({
      _id: req.params.id,
      created_by: req.user._id
    });
    if (!truck) {
      return res.status(400).json({message: 'There is no truck with such id'});
    }
    await Truck.deleteOne({_id: req.params.id, created_by: req.user._id});
    res.status(200).json({message: 'Truck deleted successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

const addTruck = async (req, res) => {
  const {type} = req.body;
  const truckType = truckTypes.find((item) => item.type === type.toUpperCase());
  if (!truckType) {
    return res.status(400).json({message: 'No truck with such type exist'});
  }
  try {
    const truckModel = new Truck({type: type.toUpperCase(), created_by: req.user._id});
    await truckModel.save();
    res.status(201).json({message: 'Truck created successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

const updateTruck = async (req, res) => {
  try {
    const truck = Truck.findOne({_id: req.params.id, created_by: req.user._id});
    if (!truck) {
      return res.status(400).json({message: 'There is no truck with such id'});
    }
    const {type} = req.body;
    const truckType = truckTypes.find((item) => item.type === type.toUpperCase());
    if (!truckType) {
      return res.status(400).json({message: 'No truck with such type exist'});
    }
    const newTruck = {
      ...truck,
      type: type.toUpperCase()
    };
    await Truck.findByIdAndUpdate(req.params.id, newTruck);
    res.status(200).json({message: 'Truck updated successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

const assignTruck = async (req, res) => {
  try {
    const truck = await Truck.findOne({_id: req.params.id, created_by: req.user._id});
    if (!truck) {
      return res.status(400).json({message: 'There is no truck with such id'});
    }
    await Truck.updateMany({created_by: req.user._id}, {'$set': {assigned_to: null, status: 'OS'}});
    await Truck.findByIdAndUpdate(req.params.id, {'$set': {assigned_to: req.user._id, status: 'IS'}});
    res.status(200).json({message: 'Truck assigned successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports = {
  getTrucks,
  getTruck,
  deleteTruck,
  addTruck,
  updateTruck,
  assignTruck
};
