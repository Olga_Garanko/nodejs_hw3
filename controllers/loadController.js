const Load = require('../models/load');
const Truck = require('../models/truck');
const {truckTypes} = require('../constants');

const getLoads = async (req, res) => {
  const {status} = req.query;
  let loads;
  try {
    if (!status) {
      loads = await Load.find({$or: [{created_by: req.user._id}, {assigned_to: req.user._id}]});
    } else {
      loads = await Load.find({
        status: status.toUpperCase(),
        $or: [{created_by: req.user._id}, {assigned_to: req.user._id}]
      });
    }
    res.status(200).json({loads});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

const getLoad = async (req, res) => {
  try {
    const load = await Load.findOne({
      _id: req.params.id,
      $or: [{created_by: req.user._id}, {assigned_to: req.user._id}]
    });
    if (!load) {
      return res.status(400).json({message: 'There is no load with such id'});
    }
    res.status(200).json({load});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

const deleteLoad = async (req, res) => {
  try {
    const load = await Load.findOne({_id: req.params.id, created_by: req.user._id});
    if (!load) {
      return res.status(400).json({message: 'There is no load with such id'});
    }
    if (load.status !== 'NEW') {
      return res.status(400).json({message: 'This load is active'});
    }
    await Load.deleteOne({_id: req.params.id, created_by: req.user._id});
    res.status(200).json({message: 'Load deleted successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

const addLoad = async (req, res) => {
  const {name, payload, width, height, length, pickup_address, delivery_address} = req.body;
  const loadModel = new Load({
    name,
    payload,
    dimensions: {width, height, length},
    delivery_address,
    pickup_address,
    created_by: req.user._id,
    logs: [{message: 'Load created successfully'}]
  });
  try {
    await loadModel.save();
    res.status(201).json({message: 'Load created successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

const updateLoad = async (req, res) => {
  try {
    const load = await Load.findOne({
      _id: req.params.id,
      created_by: req.user._id
    });
    if (!load) {
      return res.status(400).json({message: 'There is no load with such id'});
    }
    if (load.status !== 'NEW') {
      return res.status(400).json({message: 'This load is active'});
    }
    const {name, payload, width, height, length, pickup_address, delivery_address} = req.body;
    const newLogs = load.logs;
    newLogs.push({message: 'Load details changed successfully'});
    const loadModel = {
      ...load,
      name,
      payload,
      dimensions: {width, height, length},
      delivery_address,
      pickup_address,
      logs: newLogs
    };
    await Load.findByIdAndUpdate(req.params.id, loadModel);
    res.status(200).json({message: 'Load updated successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

const getActiveLoad = async (req, res) => {
  try {
    const load = await Load.findOne({status: 'ASSIGNED', assigned_to: req.user._id});
    if (!load) {
      return res.status(400).json({message: 'There is no active load'});
    }
    res.status(200).json({load});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

const setStateToLoad = async (req, res) => {
  try {
    const load = await Load.findOne({status: 'ASSIGNED', assigned_to: req.user._id});
    if (!load) {
      return res.status(400).json({message: 'There is no active load'});
    }
    let nextState;
    switch (load.state) {
      case 'En route to Pick Up':
        nextState = 'Arrived to Pick Up';
        break;
      case 'Arrived to Pick Up':
        nextState = 'En route to delivery';
        break;
      case 'En route to delivery':
        nextState = 'Arrived to delivery';
        load.logs.push({message: `Load was shipped`});
        await Load.findByIdAndUpdate(load._id, {'$set': {
          state: nextState,
          status: 'SHIPPED',
          logs: load.logs
        }});
        await Truck.findOneAndUpdate({status: 'OL', assigned_to: req.user._id}, {'$set': {status: 'IS'}});
        return res.status(200).json({message: `Load was shipped`});
      default:
        return res.status(400).json({message: 'There is no active load'});
    }
    if (nextState) {
      load.logs.push({message: `Load state changed to ${nextState}`});
      await Load.findByIdAndUpdate(load._id, {'$set': {state: nextState, logs: load.logs}});
      res.status(200).json({message: `Load state changed to ${nextState}`});
    }
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

const getShipping = async (req, res) => {
  try {
    const load = await Load.findOne({
      _id: req.params.id,
      created_by: req.user._id,
      $or: [{status: 'ASSIGNED'}, {status: 'SHIPPED'}]
    });
    if (!load) {
      return res.status(400).json({message: 'There is no active or shipped load with such id'});
    }
    const truck = await Truck.findOne({assigned_to: load.assigned_to});
    if (!truck) {
      return res.status(400).json({message: 'There is no truck assigned to this load'});
    }
    res.status(200).json({load, truck});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

const postLoad = async (req, res) => {
  try {
    const load = await Load.findOne({_id: req.params.id, created_by: req.user._id, status: 'NEW'});
    if (!load) {
      return res.status(400).json({message: 'There is no new load with such id'});
    }
    const newLogs = load.logs;
    await Load.findByIdAndUpdate(load._id, {'$set': {status: 'POSTED'}});
    const compatibleTypes = truckTypes.filter((item) => item.payload >= load.payload &&
            item.dimensions.width >= load.dimensions.width &&
            item.dimensions.height >= load.dimensions.height &&
            item.dimensions.length >= load.dimensions.length)
        .map((item) => item.type);
    if (!compatibleTypes.length) {
      newLogs.push({message: 'There is no compatible truck for this load'});
      await Load.findByIdAndUpdate(load._id, {'$set': {status: 'NEW', logs: newLogs}});
      return res.status(400).json({message: 'There is no compatible truck for this load'});
    }
    const truck = await Truck.findOne({status: 'IS', type: {$in: compatibleTypes}});
    if (!truck) {
      newLogs.push({message: 'There is no available truck for this load'});
      await Load.findByIdAndUpdate(load._id, {'$set': {status: 'NEW', logs: newLogs}});
      return res.status(400).json({message: 'There is no available truck for this load'});
    }
    newLogs.push({message: `Load was assigned to driver ${truck.assigned_to}`});
    await Load.findByIdAndUpdate(load._id, {'$set': {
      assigned_to: truck.assigned_to,
      status: 'ASSIGNED',
      state: 'En route to Pick Up',
      logs: newLogs
    }});
    await Truck.findByIdAndUpdate(truck._id, {'$set': {status: 'OL'}});
    res.status(200).json({message: 'Load posted successfully', driver_found: true});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports = {
  getLoads,
  getLoad,
  deleteLoad,
  addLoad,
  updateLoad,
  getActiveLoad,
  setStateToLoad,
  getShipping,
  postLoad
};
