const mongoose = require('mongoose');

const logsSchema = mongoose.Schema({
  message: String,
  time: {
    type: Date,
    default: new Date()
  }
}, {_id: false});

const loadSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  created_by: mongoose.Types.ObjectId,
  assigned_to: mongoose.Types.ObjectId,
  status: {
    type: String,
    default: 'NEW'
  },
  state: {
    type: String,
    default: 'On stock'
  },
  payload: Number,
  dimensions: {
    width: Number,
    height: Number,
    length: Number
  },
  pickup_address: String,
  delivery_address: String,
  logs: [logsSchema],
  created_date: {
    type: Date,
    default: new Date()
  }
});

const Load = mongoose.model('load', loadSchema);

module.exports = Load;
