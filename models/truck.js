const mongoose = require('mongoose');

const truckSchema = mongoose.Schema({
  created_by: mongoose.Types.ObjectId,
  assigned_to: mongoose.Types.ObjectId,
  status: {
    type: String,
    default: 'OS'
  },
  type: String,
  created_date: {
    type: Date,
    default: new Date()
  }
});
const Truck = mongoose.model('truck', truckSchema);

module.exports = Truck;
