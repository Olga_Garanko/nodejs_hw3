const {Router} = require('express');
const router = Router();
const {
  getLoads,
  addLoad,
  getLoad,
  deleteLoad,
  updateLoad,
  getActiveLoad,
  setStateToLoad,
  getShipping,
  postLoad
} = require('../controllers/loadController');
const {shipper} = require('../middlewares/permissionMiddleware');

const Joi = require('joi');
const validator = require('express-joi-validation').createValidator({
  passError: true
});

const querySchema = Joi.object({
  status: Joi.string()
});

const bodySchema = Joi.object({
  name: Joi.string().required(),
  payload: Joi.number().required(),
  width: Joi.number().required(),
  height: Joi.number().required(),
  length: Joi.number().required(),
  pickup_address: Joi.string().required(),
  delivery_address: Joi.string().required()
});

router.get('/', validator.query(querySchema), getLoads);
router.get('/:id', getLoad);
router.get('/active', getActiveLoad);
router.patch('/active/state', setStateToLoad);

router.use(shipper);
router.post('/', validator.body(bodySchema), addLoad);
router.delete('/:id', deleteLoad);
router.put('/:id', validator.body(bodySchema), updateLoad);
router.get('/:id/shipping_info', getShipping);
router.post('/:id/post', postLoad);

module.exports = router;
