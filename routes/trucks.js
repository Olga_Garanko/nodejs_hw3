const {Router} = require('express');
const router = Router();
const {getTrucks, addTruck, getTruck, deleteTruck, updateTruck, assignTruck} = require('../controllers/truckController');
const {driver} = require('../middlewares/permissionMiddleware');
const {enRoute} = require('../middlewares/permissionMiddleware');

const Joi = require('joi');
const validator = require('express-joi-validation').createValidator({
  passError: true
});

const bodySchema = Joi.object({
  type: Joi.string().required()
});

router.use(driver);
router.get('/', getTrucks);
router.get('/:id', getTruck);

router.use(enRoute);
router.post('/', validator.body(bodySchema), addTruck);
router.delete('/:id', deleteTruck);
router.put('/:id', validator.body(bodySchema), updateTruck);
router.post('/:id/assign', assignTruck);

module.exports = router;
