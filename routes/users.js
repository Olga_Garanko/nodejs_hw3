const {Router} = require('express');
const router = Router();
const {getUser, deleteUser, changePass} = require('../controllers/userController');
const {enRoute} = require('../middlewares/permissionMiddleware');

const Joi = require('joi');
const validator = require('express-joi-validation').createValidator({
  passError: true
});

const passwordSchema = Joi.object({
  oldPassword: Joi.string().required(),
  newPassword: Joi.string().required()
});

router.get('/me', getUser);

router.use(enRoute);
router.delete('/me', deleteUser);
router.patch('/me/password', validator.body(passwordSchema), changePass);

module.exports = router;
