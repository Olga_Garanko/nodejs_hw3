const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const usersRoutes = require('./routes/users');
const authRoutes = require('./routes/auth');
const trucksRoutes = require('./routes/trucks');
const loadsRoutes = require('./routes/loads');
// const { port, host, dbName } = require('./config/database');
const {dbUser, dbUserPassword, dbName} = require('./config/database');
const {port} = require('./config/config');

// mongoose.connect(`mongodb://${host}:${port}/${dbName}`, {
mongoose.connect(`mongodb+srv://${dbUser}:${dbUserPassword}@cluster0.lfsvz.mongodb.net/${dbName}`, {
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
});

// const logMiddleware = require('./middlewares/logMiddleware');
const logs = [];
const logMiddleware = (req, res, next) => {
  const date = new Date();
  const method = req.method;
  const url = req.url;
  const log = {
    date,
    method,
    url
  };
  logs.push(log);
  next();
};

const authMiddleware = require('./middlewares/authMiddleware');

const app = express();

app.use(express.static(path.join(__dirname, 'public')));

app.use(express.json());
app.use(logMiddleware);

app.use('/api/auth', authRoutes);

app.use(authMiddleware);
app.use('/api/users', usersRoutes);
app.use('/api/trucks', trucksRoutes);
app.use('/api/loads', loadsRoutes);

app.use((err, req, res, next) => {
  if (err && err.error && err.error.isJoi) {
    res.status(400).json({message: err.error.toString()});
  } else {
    next(err);
  }
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
